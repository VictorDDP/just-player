package jp.co.kayo.android.localplayer.service;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.util.Logger;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

public class RemoteControlReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.d("onReceive:" + intent.getAction());
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            Intent service = new Intent(context, MediaPlayerService.class);

            KeyEvent event = (KeyEvent) intent
                    .getParcelableExtra(Intent.EXTRA_KEY_EVENT);

            if (event == null || event.getAction() != KeyEvent.ACTION_DOWN)
                return;
            try {
                switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_MEDIA_STOP:
                    Logger.d("KEYCODE_MEDIA_STOP");
                    service.setAction(AppWidgetHelper.CALL_STOP);
                    context.startService(service);
                    break;
                case KeyEvent.KEYCODE_HEADSETHOOK:
                    Logger.d("KEYCODE_HEADSETHOOK");
                    // break;
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    Logger.d("KEYCODE_MEDIA_PLAY_PAUSE");
                    service.setAction(AppWidgetHelper.CALL_PLAY_PAUSE);
                    context.startService(service);
                    break;
                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    Logger.d("KEYCODE_MEDIA_NEXT");
                    service.setAction(AppWidgetHelper.CALL_FF);
                    context.startService(service);
                    break;
                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    Logger.d("KEYCODE_MEDIA_PREVIOUS");
                    service.setAction(AppWidgetHelper.CALL_REW);
                    context.startService(service);
                    break;
                default: {
                    Logger.d("不明なキーコード");
                }
                }
            } catch (Exception e) {
            }
        }
        else if("jp.co.kayo.android.localplayer.MEDIA".equals(intent.getAction())){
            int eventType = intent.getIntExtra("key.eventType", 0);
            Intent service = new Intent(context, MediaPlayerService.class);
            if(eventType == 1){
                String contentsKey = intent.getStringExtra("key.contentsKey");
                long duration = intent.getLongExtra("key.duration", 0);
                String title = intent.getStringExtra("key.title");
                String album = intent.getStringExtra("key.album");
                String artist = intent.getStringExtra("key.artist");
                String url = intent.getStringExtra("key.url");
                
                service.setAction("jp.co.kayo.android.localplayer.MEDIA");
                service.putExtra("key.eventType", eventType);
                service.putExtra("key.contentsKey", contentsKey);
                service.putExtra("key.duration", duration);
                service.putExtra("key.title", title);
                service.putExtra("key.album", album);
                service.putExtra("key.artist", artist);
                service.putExtra("key.url", url);
                context.startService(service);
            }
        }
    }
}
