package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;
import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.SongsAdapter;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.core.IProgressView;
import jp.co.kayo.android.localplayer.core.ServiceBinderHolder;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DownloadHelper;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.MediaData;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader.CallType;

@SuppressLint("NewApi")
public class PlaylistFragment extends BaseListFragment implements
        ContentManager, IProgressView, LoaderCallbacks<Cursor>,
        OnScrollListener {
    MyPreferenceManager mPref;
    ViewCache mViewCache;
    ListView mListView;
    SongsAdapter mAdapter;
    long mPlaylistId;
    String mPlaylistName;
    private AnActionModeOfEpicProportions mActionMode;
    Runnable mTask = null;
    private int mLoaderId = -1;

    ViewCache getCache() {
        if (mViewCache == null) {
            mViewCache = (ViewCache) getFragmentManager().findFragmentByTag(
                    SystemConsts.TAG_CACHE);
        }
        return mViewCache;
    }

    private IMediaPlayerService getBinder() {
        if (getActivity() instanceof ServiceBinderHolder) {
            return ((ServiceBinderHolder) getActivity()).getBinder();
        } else {
            return null;
        }
    }

    public static PlaylistFragment createFragment(long playlistId,
            String playlistName, Bundle args) {
        PlaylistFragment f = new PlaylistFragment();
        args.putLong("playlistId", playlistId);
        args.putString("playlistname", playlistName);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mPref == null) {
            mPref = new MyPreferenceManager(getActivity());
        }

        Bundle args = getArguments();
        if (args != null) {
            mPlaylistId = args.getLong("playlistId");
            mPlaylistName = args.getString("playlistname");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.songs_view, null);

        mListView = (ListView) root.findViewById(android.R.id.list);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        mListView.setOnScrollListener(this);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
        mLoaderId = -1;
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        if (mActionMode.hasMenu()) {
            mActionMode.onItemClick(l, v, position, id);
        } else {
            BaseActivity base = (BaseActivity) getActivity();
            final IMediaPlayerService binder = base.getBinder();
            if (binder == null) {
                return;
            }
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    playMediaReplace(SystemConsts.CONTENTSKEY_PLAYLIST
                            + mPlaylistId, binder, position);
                    return null;
                }
            };
            task.execute();

            if (mPref.isClickAndBack()) {
                base.getSupportFragmentManager().popBackStack();
                mPref.setResumeReloadFlag(true);
            }
        }
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_remove))
                    .setIcon(R.drawable.ic_menu_remove)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_artist))
                    .setIcon(R.drawable.ic_menu_btn_artist)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.sub_mnu_album))
                    .setIcon(R.drawable.ic_menu_album)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            boolean b = mPref.useLastFM();
            if (b) {
                menu.add(getString(R.string.sub_mnu_love)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
                menu.add(getString(R.string.sub_mnu_ban)).setShowAsAction(
                        MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_refresh)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }

    };

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
            case SystemConsts.EVT_SELECT_RATING: {
                Long[] ids = new Long[items.size()];
                for (int i = 0; i < items.size(); i++) {
                    int index = items.get(i);
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(index);
                    if (selectedCursor != null) {
                        int audioIdCol = selectedCursor
                                .getColumnIndex(MediaConsts.AudioPlaylistMember.AUDIO_ID);
                        if (audioIdCol == -1) {
                            audioIdCol = selectedCursor
                                    .getColumnIndex(MediaConsts.Media._ID);
                        }

                        long mediaId = selectedCursor.getLong(audioIdCol);
                        ids[i] = mediaId;
                    }
                }
                RatingDialog dlg = new RatingDialog(getActivity(),
                        TableConsts.FAVORITE_TYPE_SONG, ids, mHandler);
                dlg.show();
            }
                break;
            case SystemConsts.EVT_SELECT_ADD: {
                long[] ids = new long[items.size()];
                for (int i = 0; i < items.size(); i++) {
                    int index = items.get(i);
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(index);
                    if (selectedCursor != null) {
                        int audioIdCol = selectedCursor
                                .getColumnIndex(MediaConsts.AudioPlaylistMember.AUDIO_ID);
                        if (audioIdCol == -1) {
                            audioIdCol = selectedCursor
                                    .getColumnIndex(MediaConsts.Media._ID);
                        }

                        long mediaId = selectedCursor.getLong(audioIdCol);
                        ids[i] = mediaId;
                    }
                }

                AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(
                        getActivity(), getFragmentManager(), ids,
                        CallType.TYPE_ADDDIALOG);
                task.execute();
            }
                break;
            case SystemConsts.EVT_SELECT_DEL: {
                final ArrayList<String> whereArgs = new ArrayList<String>();
                final StringBuilder where = new StringBuilder();
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        long playlistmediaId = selectedCursor
                                .getLong(selectedCursor
                                        .getColumnIndex(MediaConsts.AudioPlaylistMember._ID));
                        if (where.length() > 0) {
                            where.append(" OR ");
                        }
                        where.append(AudioMedia._ID + " = ?");
                        whereArgs.add(Long.toString(playlistmediaId));
                    }
                }

                final Uri playlisturi = ContentUris.withAppendedId(
                        MediaConsts.PLAYLIST_CONTENT_URI, mPlaylistId);
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());
                builder.setTitle(getString(R.string.lb_confirm));
                builder.setMessage(String.format(
                        getString(R.string.fmt_remove_orderlist),
                        Integer.toString(items.size())));
                builder.setPositiveButton(getString(R.string.lb_ok),
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                // delte proc
                                getActivity().getContentResolver().delete(
                                        playlisturi,
                                        where.toString(),
                                        whereArgs.toArray(new String[whereArgs
                                                .size()]));
                                reload();
                            }
                        });
                builder.setNegativeButton(getString(R.string.lb_cancel), null);
                builder.setCancelable(true);
                AlertDialog dlg = builder.create();
                dlg.show();
            }
                break;
            case SystemConsts.EVT_SELECT_CLEARCACHE: {
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        int audioIdCol = selectedCursor
                                .getColumnIndex(MediaConsts.AudioPlaylistMember.AUDIO_ID);
                        if (audioIdCol == -1) {
                            audioIdCol = selectedCursor
                                    .getColumnIndex(MediaConsts.Media._ID);
                        }

                        long mediaId = selectedCursor.getLong(audioIdCol);
                        String data = selectedCursor.getString(selectedCursor
                                .getColumnIndex(MediaConsts.AudioMedia.DATA));

                        ContentsUtils.clearMediaCache(getActivity(), data);
                        ContentValues values = new ContentValues();
                        values.put(TableConsts.AUDIO_CACHE_FILE, (String) null);
                        getActivity()
                                .getContentResolver()
                                .update(ContentUris.withAppendedId(
                                        MediaConsts.MEDIA_CONTENT_URI, mediaId),
                                        values, null, null);
                    }
                }
                datasetChanged();
            }
                break;
            case SystemConsts.EVT_SELECT_EDIT: {
                long[] ids = new long[items.size()];
                for (int i = 0; i < items.size(); i++) {
                    int pos = items.get(i);
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(pos);
                    if (selectedCursor != null) {
                        int audioIdCol = selectedCursor
                                .getColumnIndex(MediaConsts.AudioPlaylistMember.AUDIO_ID);
                        if (audioIdCol == -1) {
                            audioIdCol = selectedCursor
                                    .getColumnIndex(MediaConsts.Media._ID);
                        }

                        long mediaId = selectedCursor.getLong(audioIdCol);
                        ids[i] = mediaId;
                    }
                }
                Intent intent = new Intent(getActivity(), TagEditActivity.class);
                intent.putExtra(SystemConsts.KEY_EDITTYPE,
                        TagEditActivity.MEDIA);
                intent.putExtra(SystemConsts.KEY_EDITKEYLIST, ids);
                getActivity().startActivityForResult(intent,
                        SystemConsts.REQUEST_TAGEDIT);
            }
                break;
            case SystemConsts.EVT_SELECT_LOVE:
            case SystemConsts.EVT_SELECT_BAN: {
                for (Integer i : items) {
                    messageHandle(what, i);
                }
            }
                break;
            case SystemConsts.EVT_SELECT_CHECKALL: {
                for (int i = 0; i < mListView.getCount(); i++) {
                    mListView.setItemChecked(i, true);
                }
            }
                break;
            default: {
                messageHandle(what, items.get(0));
            }
            }
        }
    }

    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            long mediaId = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(AudioPlaylistMember.AUDIO_ID));
            String title = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.TITLE));
            String album = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ALBUM));
            String artist = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.ARTIST));
            String data = selectedCursor.getString(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.DATA));
            long duration = selectedCursor.getLong(selectedCursor
                    .getColumnIndex(MediaConsts.AudioMedia.DURATION));

            switch (what) {
            case SystemConsts.EVT_SELECT_ARTIST: {
                if (artist != null) {
                    FragmentTransaction t = getFragmentManager()
                            .beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(mPref, t);
                    t.replace(
                            getId(),
                            jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                    .createFragment(null, artist, -1,
                                            FragmentUtils.cloneBundle(this)));
                    t.addToBackStack(SystemConsts.TAG_SUBCHILDFRAGMENT);
                    t.hide(this);
                    t.commit();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_OPENALBUM: {
                Hashtable<String, String> tbl = ContentsUtils.getMedia(
                        getActivity(), new String[] { AudioMedia.ALBUM_KEY,
                                AudioMedia.ALBUM }, mediaId);
                String album_key = tbl.get(AudioMedia.ALBUM_KEY);
                if (album_key != null) {
                    String album_name = tbl.get(AudioMedia.ALBUM);
                    FragmentTransaction t = getFragmentManager()
                            .beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(mPref, t);
                    t.replace(getId(), AlbumSongsFragment.createFragment(
                            album_key, album_name,
                            FragmentUtils.cloneBundle(this)));
                    t.addToBackStack(SystemConsts.TAG_SUBCHILDFRAGMENT);
                    t.hide(this);
                    t.commit();
                }
            }
                break;
            case SystemConsts.EVT_SELECT_MORE: {
                showInfoDialog(album, artist, title);
            }
                break;
            case SystemConsts.EVT_SELECT_DOWNLOAD: {
                DownloadHelper helper = new DownloadHelper(getActivity());
                try {
                    if (helper.enqueueMusic(mediaId, title, data) != -1) {
                        Toast.makeText(getActivity(),
                                getString(R.string.txt_action_download),
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (MalformedURLException e) {
                    Logger.e("Create Uri failed.", e);
                }
            }
                break;
            case SystemConsts.EVT_SELECT_LOVE: {
                ContentsUtils.lastfmLove(getActivity(), title, artist, album,
                        Long.toString(duration));
            }
                break;
            case SystemConsts.EVT_SELECT_BAN: {
                ContentsUtils.lastfmBan(getActivity(), title, artist, album,
                        Long.toString(duration));
            }
                break;
            default: {
            }
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        if (mAdapter == null) {
            mAdapter = new SongsAdapter(getActivity(), null, getCache());

            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        showProgressBar();
        return new CursorLoader(getActivity(), ContentUris.withAppendedId(
                MediaConsts.PLAYLIST_CONTENT_URI, mPlaylistId), null,
                selection, selectionArgs, AudioPlaylistMember.PLAY_ORDER);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        hideProgressBar();
        if (mAdapter != null && data != null && !data.isClosed()) {
            Cursor cur = mAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
    }

    public ArrayList<MediaData> getIds() {
        ArrayList<MediaData> ret = new ArrayList<MediaData>();
        int count = mAdapter.getCount();
        for (int i = 0; i < count; i++) {
            Cursor cursor = (Cursor) mAdapter.getItem(i);
            int audioIdCol = cursor
                    .getColumnIndex(MediaConsts.AudioPlaylistMember.AUDIO_ID);
            if (audioIdCol == -1) {
                audioIdCol = cursor.getColumnIndex(MediaConsts.Media._ID);
            }
            long id = cursor.getLong(audioIdCol);
            String data = cursor.getString(cursor
                    .getColumnIndex(MediaConsts.AudioPlaylistMember.DATA));
            ret.add(new MediaData(id, data));
        }
        return ret;
    }

    public boolean playMediaReplace(String contentkey,
            IMediaPlayerService binder, int position) {
        try {
            if (!binder.setContentsKey(contentkey)) {
                int pos = binder.getPosition();
                if (position != pos) {// 同じ位置をクリックした
                    binder.lockUpdateToPlay();
                    try {
                        binder.reset();
                        binder.setPosition(position);
                    } finally {
                        binder.play();
                    }
                    return true;
                } else {
                    int stat = binder.stat();
                    if ((stat & AppWidgetHelper.FLG_PLAY) == 0) {
                        binder.play();
                    }
                }
            } else {
                String selection = null;
                String[] selectionArgs = null;

                binder.playMediaQuery(
                        ContentUris.withAppendedId(
                                MediaConsts.PLAYLIST_CONTENT_URI, mPlaylistId)
                                .toString(),
                        MediaConsts.AudioPlaylistMember.AUDIO_ID,
                        MediaConsts.AudioPlaylistMember.DATA, selection,
                        selectionArgs, AudioPlaylistMember.PLAY_ORDER, position);
                return true;
            }
        } catch (RemoteException e) {
        }
        return false;
    }

    @Override
    public void startProgress(final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    getCache().startProgress(max);
                    IMediaPlayerService binder = getBinder();
                    if (binder != null) {
                        try {
                            getCache().setPrefetchId(binder.getPrefetchId());
                        } catch (RemoteException e) {
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void stopProgress() {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    getCache().stopProgress();
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void progress(final long pos, final long max) {
        if (mAdapter != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    getCache().progress(pos, max);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void release() {

    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        // TODO Auto-generated method stub

    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    protected void datasetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void addRow(Object[] values) {
    }

    @Override
    public String getName(Context context) {
        StringBuilder buf = new StringBuilder();
        buf.append(context.getString(R.string.lb_tab_playlist_songs_name));
        if (mPlaylistName != null) {
            buf.append(mPlaylistName);
        }
        return buf.toString();
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        getFragmentManager().popBackStack();
        return null;
    }

    @Override
    public int getFragmentId() {
        if (mLoaderId == -1) {
            mLoaderId = new Random(System.currentTimeMillis())
                    .nextInt(99999999);
        }
        return mLoaderId;
    }
}
