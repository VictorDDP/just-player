package jp.co.kayo.android.localplayer.menu;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.BaseActivity;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;

import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MultipleChoiceMediaContentActionCallback implements Callback, OnItemLongClickListener, OnItemClickListener {

    protected Context mContext;
    protected Handler mHandler;
    protected GridView mGridView;
    protected ListView mListView;
    protected ActionMode mMode;
    protected int mSelectedIndex;
    public boolean hasChoice;

    public MultipleChoiceMediaContentActionCallback(Context context,
            GridView gridView, Handler handler) {
        mContext = context;
        mHandler = handler;
        mGridView = gridView;
        mGridView.setOnItemLongClickListener(this);
        hasChoice = MultipleChoiceMediaContentActionCallback
                .haveChoice(gridView);
        if(hasChoice){
            mGridView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    public MultipleChoiceMediaContentActionCallback(Context context,
            ListView listView, Handler handler) {
        mContext = context;
        mHandler = handler;
        mListView = listView;
        mListView.setOnItemLongClickListener(this);
        hasChoice = MultipleChoiceMediaContentActionCallback
                .haveChoice(listView);
        if(hasChoice){
            mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    public static boolean haveChoice(Object object) {
        Method method;
        try {
            method = object.getClass().getMethod("setChoiceMode",
                    new Class[] { int.class });
            if (method != null) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        int what = -1;
        if (item.getTitle().equals(mContext.getString(R.string.sub_mnu_rating))) {
            what = SystemConsts.EVT_SELECT_RATING;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_play))) {
            what = SystemConsts.EVT_SELECT_PLAY;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_order))) {
            what = SystemConsts.EVT_SELECT_ADD;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_album))) {
            what = SystemConsts.EVT_SELECT_OPENALBUM;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.txt_web_more))) {
            what = SystemConsts.EVT_SELECT_MORE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_albumart))) {
            what = SystemConsts.EVT_SELECT_ALBUMART;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_download))) {
            what = SystemConsts.EVT_SELECT_DOWNLOAD;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_clearcache))) {
            what = SystemConsts.EVT_SELECT_CLEARCACHE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.txt_web_lyrics))) {
            what = SystemConsts.EVT_SELECT_LYRICS;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.txt_web_youtube))) {
            what = SystemConsts.EVT_SELECT_YOUTUBE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_love))) {
            what = SystemConsts.EVT_SELECT_LOVE;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_ban))) {
            what = SystemConsts.EVT_SELECT_BAN;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_remove))) {
            what = SystemConsts.EVT_SELECT_DEL;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_artist))) {
            what = SystemConsts.EVT_SELECT_ARTIST;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_edit))) {
            what = SystemConsts.EVT_SELECT_EDIT;
        } else if (item.getTitle().equals(
                mContext.getString(R.string.sub_mnu_selectall))) {
            what = SystemConsts.EVT_SELECT_CHECKALL;
        }

        if(hasChoice){
            Message msg = mHandler.obtainMessage(what, makeSelectedItems());
            mHandler.sendMessage(msg);
        }
        else{
            ArrayList<Integer> list = new ArrayList<Integer>();
            list.add(mSelectedIndex);
            Message msg =  mHandler.obtainMessage(what, list);
            mHandler.sendMessage(msg);
        }
        return true;
    }

    public Object makeSelectedItems() {
        List<Integer> list = new ArrayList<Integer>();
        SparseBooleanArray checked = null;
        if (mListView != null) {
            checked = mListView.getCheckedItemPositions();
        } else {
            checked = mGridView.getCheckedItemPositions();
        }
        if (checked != null) {
            for (int i = 0; i < checked.size(); i++) {
                if (checked.valueAt(i))
                    list.add(checked.keyAt(i));
            }
        }
        return list;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        setActionMode(null);
    }

    public boolean hasMenu() {
        return mMode != null;
    }

    public int getCheckedItemCount() {
        if (Build.VERSION.SDK_INT >= 11) {
            if (mListView != null) {
                return mListView.getCheckedItemCount();
            } else {
                return mGridView.getCheckedItemCount();
            }
        } else {
            long[] ids = null;
            if (mListView != null) {
                ids = mListView.getCheckItemIds();
            } else {
                ids = mGridView.getCheckedItemIds();
            }
            if (ids != null) {
                return ids.length;
            } else {
                return 0;
            }
        }
    }

    public boolean cancelActionMode() {
        if (mMode != null) {
            mMode.finish();
            if(hasChoice){
                if (mListView != null) {
                    mListView.setSelection(-1);
                } else {
                    mGridView.setSelection(-1);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        if(hasChoice){
            if (mMode != null) {
                selectItem(-1);
            }
        }
        else{
            mSelectedIndex = arg2;
            View childView = null;
            if(mListView!=null){
                childView = mListView.getChildAt(arg2);
            }else{
                childView = mGridView.getChildAt(arg2);
            }
            if(childView!=null){
                View view2 = childView.findViewById(R.id.text2);
                if(view2!=null){
                    mMode.setTitle(((TextView)view2).getText().toString());
                }
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View view,
            int position, long arg3) {
        if(hasChoice){
            Object selectedItem = getItem(position);
            if (selectedItem != null) {
                BaseActivity act = (BaseActivity) mContext;
                setActionMode(act.startActionMode(this));
                selectItem(position);
                return true;
            }
        }
        else{
            Object selectedItem = getItem(position);
            if (selectedItem != null) {
                BaseActivity act = (BaseActivity)mContext;
                mMode = act.startActionMode(this);
                mSelectedIndex = position;
                
                View view2 = view.findViewById(R.id.text2);
                if(view2!=null){
                    mMode.setTitle(((TextView)view2).getText().toString());
                }else{
                    if(mContext instanceof ContentManager){
                        mMode.setTitle(((ContentManager)mContext).getName(mContext));
                    }else{
                        mMode.setTitle(mContext.getString(R.string.txt_selected_title));
                    }
                }
                mMode.setSubtitle(String.format(mContext.getString(R.string.fmt_selected_items), 1));
                return true;
            }
        }

        return false;
    }

    public Object getItem(int position) {
        if (mListView != null) {
            return mListView.getAdapter().getItem(position);
        } else {
            return mGridView.getAdapter().getItem(position);
        }
    }

    protected void setActionMode(ActionMode mode) {
        mMode = mode;
        if(hasChoice){
            if (mMode != null) {
                if (mListView != null) {
                    mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                } else {
                    mGridView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                }
            } else {
                if (mListView != null) {
                    mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    mListView.clearChoices();
                    mListView.requestLayout();
                } else {
                    mGridView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    mGridView.clearChoices();
                    mGridView.requestLayout();
                }
            }
        }
    }
    
    public void unselectItem(int selectPosition) {
        if (selectPosition != -1) {
            if (mListView != null) {
                mListView.setItemChecked(selectPosition, false);
                mListView.requestLayout();
            } else {
                mGridView.setItemChecked(selectPosition, false);
                mGridView.requestLayout();
            }
        }
        int count = getCheckedItemCount();
        if (selectPosition != -1 || count > 0) {
            if (mContext instanceof ContentManager) {
                mMode.setTitle(((ContentManager) mContext).getName(mContext));
            } else {
                mMode.setTitle(mContext.getString(R.string.txt_selected_title));
            }
            mMode.setSubtitle(String.format(
                    mContext.getString(R.string.fmt_selected_items), count));
        } else {
            cancelActionMode();
        }
    }

    public void selectItem(int selectPosition) {
        if (selectPosition != -1) {
            if (mListView != null) {
                mListView.clearChoices();
                mListView.setItemChecked(selectPosition, true);
                mListView.requestLayout();
            } else {
                mGridView.clearChoices();
                mGridView.setItemChecked(selectPosition, true);
                mGridView.requestLayout();
            }
        }
        int count = getCheckedItemCount();
        if (selectPosition != -1 || count > 0) {
            if (mContext instanceof ContentManager) {
                mMode.setTitle(((ContentManager) mContext).getName(mContext));
            } else {
                mMode.setTitle(mContext.getString(R.string.txt_selected_title));
            }
            mMode.setSubtitle(String.format(
                    mContext.getString(R.string.fmt_selected_items), count));
        } else {
            cancelActionMode();
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        // TODO Auto-generated method stub
        return false;
    }
}
