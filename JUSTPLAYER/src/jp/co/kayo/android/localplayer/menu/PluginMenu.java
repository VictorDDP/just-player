package jp.co.kayo.android.localplayer.menu;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.util.Funcs;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import android.view.MenuItem;
import android.view.SubMenu;
import android.view.MenuItem.OnMenuItemClickListener;

public class PluginMenu extends BaseActionProvider implements OnMenuItemClickListener {
    IMediaPlayerService binder;
    List<ResolveInfo> resolves;

    public PluginMenu(Context context) {
        super(context);
    }

    public void setBinder(IMediaPlayerService binder) {
        this.binder = binder;
    }

    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        
        //sleep tiemr
        subMenu.addSubMenu(R.id.mnu_plugin, R.string.lb_sleeptimer, 0,
                context.getString(R.string.lb_sleeptimer));
        

        PackageManager pm = context.getPackageManager();
        resolves = Funcs.getPlugin(context);
        for (int i = 0; i < resolves.size(); i++) {
            ResolveInfo inf = resolves.get(i);
            CharSequence labelSeq = inf.loadLabel(pm);
            CharSequence label = labelSeq != null
                    ? labelSeq
                    : inf.activityInfo.name;

            subMenu.addSubMenu(R.id.mnu_plugin, R.string.lb_sleeptimer + (i + 1), i + 1, label);
        }
        subMenu.addSubMenu(R.id.mnu_plugin, R.string.lb_sleeptimer + (resolves.size() + 2), resolves.size() + 2,
                context.getString(R.string.lb_conf_add_plugin_title));

        for (int i = 0; i < subMenu.size(); ++i) {
            subMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemid = item.getItemId();
        doItemSelect(itemid, item.getTitle().toString());
        return true;
    }

    @Override
    public boolean onPerformDefaultAction() {
        if (Build.VERSION.SDK_INT < 11) {
            ArrayList<String> menuItems = new ArrayList<String>();
            
            //sleep timer
            menuItems.add(context.getString(R.string.lb_sleeptimer));
            
            PackageManager pm = context.getPackageManager();
            resolves = Funcs.getPlugin(context);
            for (int i = 0; i < resolves.size(); i++) {
                ResolveInfo inf = resolves.get(i);
                CharSequence labelSeq = inf.loadLabel(pm);
                CharSequence label = labelSeq != null
                        ? labelSeq
                        : inf.activityInfo.name;

                menuItems.add(label.toString());
            }
            menuItems.add(context.getString(R.string.lb_conf_add_plugin_title));


            final CharSequence[] items = menuItems.toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.mnu_plugin))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            doItemSelect(item, items[item].toString());
                        }
                    })
                    .show();
        }
        return super.onPerformDefaultAction();
    }

    
    private void doItemSelect(int itemid, String item_name){
        if (item_name.equals(context.getString(R.string.lb_conf_add_plugin_title))) {
            Uri uri = Uri
                    .parse("market://search?q=jp.co.kayo.android.localplayer.plugin");
            Intent it = new Intent(Intent.ACTION_VIEW, uri);
            try{
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(it);
            }catch(Exception e){
                //Marketが入っていない
                uri = Uri.parse("https://bitbucket.org/yokmama/just-player-plugins/downloads");
                Intent i = new Intent(Intent.ACTION_VIEW,uri);
                context.startActivity(i);
            }
        }
        else if (item_name.equals(context.getString(R.string.lb_sleeptimer))) {
            Intent it = new Intent(context, jp.co.kayo.android.localplayer.plugin.sleeptimer.MainActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(it);
        }
        else {
            Intent it = new Intent();
            it.setClassName(
                    resolves.get(itemid).activityInfo.packageName,
                    resolves.get(itemid).activityInfo.packageName
                            + ".MainActivity");
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(it);
        }
    }
}
