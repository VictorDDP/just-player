package jp.co.kayo.android.localplayer.widget;

import jp.co.kayo.android.localplayer.R;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class CheckableFrameLayout extends FrameLayout implements Checkable {
    private boolean checked;

    public CheckableFrameLayout(Context context) {
        super(context);
    }

    public CheckableFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CheckableFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setChecked(boolean checked) {
        if (this.checked != checked) {
            this.checked = checked;

            // 背景色を選択状態によって変更する
            if (this.checked) {
                setBackgroundResource(R.color.list_selectedcolor);
            } else {
                setBackgroundResource(android.R.color.transparent);
            }
            
            refreshDrawableState();
        }
    }

    @Override
    public boolean isChecked() {
        return checked;
    }

    @Override
    public void toggle() {
        setChecked(!checked);
    }

}
